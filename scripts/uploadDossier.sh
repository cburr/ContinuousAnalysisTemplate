#!/bin/bash

rm -rf $WORKDIR/Dossier && mkdir $WORKDIR/Dossier
git clone ssh://git@gitlab.cern.ch:7999/sneubert/CAT-Dossier.git $WORKDIR/Dossier/
exiv2 -M"set Exif.Photo.UserComment charset=Ascii Created from $CI_BUILD_REPO with ref $CI_BUILD_REF" $WORKDIR/*.png 
cp $WORKDIR/*.png $WORKDIR/Dossier/plots/
$WORKDIR/Dossier/buildDossier.py $WORKDIR/Dossier/ README.jinja $WORKDIR/Dossier/README.md
$WORKDIR/Dossier/buildDossier.py latex signatures.jinja $WORKDIR/Dossier/latex/signatures.tex
cd $WORKDIR/Dossier/latex
lualatex CAT.tex
cd $WORKDIR/Dossier/
git add plots/*.png
git add latex/CAT.pdf
git add README.md
git diff-index --quiet HEAD || ( git commit -m "updated plots" &&  git push )
     

# Setting up Continuous Integration

A continuous integration (CI) server will run your analysis whenever you make a change to the code. 

## Getting to know CI
The simplest way to get to know how it works is to for this template and run its CI.
Just follow these steps (most of the settings should be there by default):

### Enabling a CI runner for a forked project:
* In your fork go to `Settings` menu
* In the `Project Settings` look for the section on  continuos integration
* Switch the option to `git clone`
* Go to the `Runners` menu
* You should see one of my runners there with a key looking like `f5dbbc75`
* Click `ENABLE FOT THIS PROJECT`
* **Click `DISABLE SHARED RUNNERS`**
* When you go back to the project dashboard there now should be a new menu item `Builds`, showing `[0]` builds

If you don't find an available runner have a look at the [instructions](https://gitlab.cern.ch/sneubert/ANA-Docker) how to set one up for yourselve.

### Your first build
Simply push a change to your build. For example you can edit [fitconfig.info](config/fitconfig.info) and change this cut:
```
selection "y > -10"
```
Commit and push to your fork. When you refresh your gitlab project homepage you should notice an icon telling you that a build is running:
![CI running](pics/CIrunning.png)

Then go into the `Builds` menu. This should look something like this. If you don't see a job, check the `finished` tab.
![Build](pics/Buildrunning.png)

Clicking on the build number let's you see the build-log (in realtime!):
![BuildLog](pics/BuildLog.png)
Note how only those parts of the pipline are run where you changed something!

When everything goes alright you should be able to download the results of the job by clicking the button next to the build log
![artifatcs](pics/artifacts.png)


### Configuration of the CI builds
This is done in [.gitlab-ci.yml](.gitlab-ci.yml). Detailed documentation is available at [gitlab-ci homepage](http://doc.gitlab.com/ce/ci/yaml/README.html)

## Setting up your own server
Detailed instructions are available in the [ANA-Docker](https://gitlab.cern.ch/sneubert/ANA-Docker) package.

## Advanced topics:
These issues have been solved for the Lb2LcD0K analysis and will be documented here
* Coordinating several developers on the same runner
* Uploading results into a "Dossier" repository
* Tagging plots with meta-information

New ideas:
* In LaTex: Linking results back to the code on the repo
